<?php
// Блок с функциями sites.my1.ru

$DataBase = null;
$Num=1;
$NumCat=0;

// Загрузка базы с сайтами
function LoadBase()
{
	if (!file_exists("base/base.txt")) return null;
	$FileBase = file_get_contents("base/base.txt");
	return $FileBase;
}

// Поиск сайтов в файле и преобразование в массив
function Init(){
	$Base = LoadBase();
	if ($Base==null) {echo "Внимание, проблема с базой данных!";}
		else {
			preg_match_all('/"([^;"]*)"/', $Base,$out,0);
			return $out;
		}
}

// Перебор значений массива и определение типа значения
function Menu(){
	global $DataBase;
	$Sites = Init();
	
	foreach ($Sites[0] as $value) {
		if (($value=='","') or ($value=='":["') or ($value=='"],"') ) continue;
		$value = substr($value, 1,strlen($value)-2);
		if (strpos($value,':')==0) BildMenu($value);
			else BildSubMenu($value);
			
	}
	BildMenu("Возврат на главную",1);
}

// постройка меню
function BildMenu($TextMenu,$id=0){
	global $Num;
	global $NumCat;
	$NumCat++;
	$Num=1;
		if ($id==0) $id = $NumCat+1;
	$Text = '<a id="'.($NumCat).'"></a><div class="Menu"><a href="#'.($id).'">'.$TextMenu.'</div>';
	echo $Text;
}

// постройка ссылки
function BildSubMenu($TextMenu){
	global $Num;
	
	$HrefText = substr($TextMenu,strpos($TextMenu,'/')+2,strlen($TextMenu)-strpos($TextMenu,'/'));
	$Text = '<div class="submenu">'.($Num++).': <a href="'.$TextMenu.'">'.$HrefText.'</a></div>';
	echo $Text;
}





